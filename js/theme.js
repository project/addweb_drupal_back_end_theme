/**
 * @file
 * Nominal Admin script
 */

(function ($) {
  'use strict';

  Drupal.behaviors.nominal = {
    attach: function (context, settings) {
      // Custom checkbox and radio design
      $('input[type="radio"], input[type="checkbox"]').wrap('<div class="input-rc"></div>');
      $('.input-rc').append('<span class="input-rc-span"></span>');
      // For disabled
      $('input[disabled="disabled"]').siblings('.input-rc-span').addClass('input-rc-disabled');
      // Permission page
      $('.permissions input[type=checkbox].dummy-checkbox').each(function () {
        $(this).parent().addClass('input-rc-none');
      });

      // Responsive table
      $('table').wrap('<div class="responsive-tbl"></div>');
    }
  };

}(jQuery));
