----------
QUICK INFO
----------
-> Custom responsive admin theme for Drupal 8.
-> Improved and easy to use UI & UX.
-> Cross browser compatibility.
-> Admin menu in this theme has simple and easy hierarchy. It highlights the 
inner menu level in a special way to clearly understand the menu levels.
-> The vertical menu on the left makes it easy for the users to select various 
functionalities of the Admin menu.
-> Table design has been made more convenient and readable by enhancing the 
look and feel of different sections.
-> Visibility of the entire menu has been made better by using decent colours.
-> Made titles & icons visually prominent.
-> The user experience for admin, manager, editor and others has improved a 
lot because of the simple and elegant look of the Admin theme.
-> User friendly browsing experience in menus, buttons and inner menus 
specially for our users.
-> The sophisticated design of the pop-up menu marks for better and easy usage.
-> We have tried to give a stylish look to the entire theme to make it a 
memorable browsing experience for the users.

-----------------------
GULP INSTALLATION STEPS
-----------------------
TO INSTALL nodejs & npm GLOBALLY (Skip this is already installed)
For Windows OS and Mac OS
Download node js from 'https://nodejs.org/en/download/'
For Linux OS: 'sudo apt-get install npm'

TO INSTALL gulp
Open path from terminal: ..\projects_name\themes\nominal_admin_theme

-> npm install gulp (For Linux: sudo install gulp -g)
-> npm install gulp-sass --save-dev
-> npm install gulp-concat --save-dev

After installation is completed run 'gulp' command
